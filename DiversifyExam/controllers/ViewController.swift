//
//  ViewController.swift
//  DiversifyExam
//
//  Created by Joseph Daryl Locsin on 07/11/2017.
//  Copyright © 2017 Joseph Daryl Locsin. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class ViewController: UIViewController {
    
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var labelFilterDistance: UILabel!
    @IBOutlet private weak var sliderFilterDistance: UISlider!
    @IBOutlet private weak var tableView: UITableView!
    
    private var service = RestaurantService()
    
    private var locationManager: CLLocationManager!
    
    private var isLocationFixAchieved: Bool = false
    
    private var currentSearchMode: RestaurantService.SearchMode = .Ranked
    private var currentSearchRadius: Float = 0
    private var currentSearchString: String?
    private var currentUserLocation: CLLocation?
    private var currentSelectedRestaurant: Restaurant?
    
    private var restosSearchedByRank = [Restaurant]()
    private var restosSearchedByRadius = [Restaurant]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
        self.configureLocationManager()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DetailVC {
            vc.restaurant = self.currentSelectedRestaurant!
            vc.userLocation = self.currentUserLocation
        }
    }
    
    private func configureUI() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        
        self.sliderFilterDistance.addTarget(self, action: #selector(self.sliderValueChanged(some:)), for: UIControlEvents.touchUpInside)
        self.sliderFilterDistance.minimumValue = 0
        self.sliderFilterDistance.maximumValue = 50000
        self.currentSearchRadius = self.sliderFilterDistance.value
    }
    
    private func configureLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    @objc private func sliderValueChanged(some: UISlider) {
        print("slider value changed: \(some.value)")
        self.currentSearchRadius = some.value
        self.labelFilterDistance.text = "\(self.currentSearchRadius) meters"
    }
    
    private func searchBySearchString() {
        if let searchStr = self.currentSearchString, let userLocation = self.currentUserLocation {
            self.currentSearchMode = .Radius(some: Double(self.currentSearchRadius))
            self.service.fetchNearbyRestaurants(from: userLocation.coordinate, searchString: searchStr, mode: self.currentSearchMode, completion: { (someRestos, someError) in
                
                DispatchQueue.main.async {
                    if let list = someRestos, list.count > 0 {
                        self.restosSearchedByRadius = list
                        self.tableView.reloadData()
                    } else {
                        self.presentAlertMessage(someMessage: "No results found", someTitle: "Sorry!")
                    }
                }
            })
        }
    }
    
    private func searchTopNearbyRestos() {
        if let userLocation = self.currentUserLocation {
            self.currentSearchMode = .Ranked
            self.service.fetchNearbyRestaurants(from: userLocation.coordinate, searchString: nil, mode: self.currentSearchMode, completion: { (someRestos, someError) in
                
                DispatchQueue.main.async {
                    if let list = someRestos, list.count > 0 {
                        self.restosSearchedByRank = list
                        self.tableView.reloadData()
                    } else {
                        self.presentAlertMessage(someMessage: "Please try again later.", someTitle: "Uh-oh! Something went wrong.")
                    }
                }
            })
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.isLocationFixAchieved == false {
            self.isLocationFixAchieved = true // only get user loc, once for now
            
            if let lastLocation = locations.last {
                self.currentUserLocation = lastLocation
//                self.currentUserLocation = CLLocation(latitude: 14.5666087, longitude: 121.0195967)
                self.searchTopNearbyRestos()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("[Location] requesting access...")
            self.locationManager.requestWhenInUseAuthorization()
            
        case .restricted, .denied:
            print("[Location] denied access - reason: \(status)")
            
        case .authorizedWhenInUse:
            print("[Location] updating...")
            self.locationManager.startUpdatingLocation()
            
        case .authorizedAlways:
            print("[Location] not handled")
            break
        }
    }
}

extension ViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchStr = searchBar.text, searchStr.characters.count > 0 {
            self.currentSearchString = searchStr
            self.searchBySearchString()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        
        if let someText = searchBar.text {
            if someText.characters.count == 0 {
                self.searchTopNearbyRestos()
            }
        } else {
            self.searchTopNearbyRestos()
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let _cell = tableView.dequeueReusableCell(withIdentifier: RestaurantCell.cellIdentifier(), for: indexPath)
        
        if let cell = _cell as? RestaurantCell {
            
            var resto: Restaurant!
            
            switch self.currentSearchMode {
            case .Radius:
                resto = self.restosSearchedByRadius[indexPath.row]
            case .Ranked:
                resto = self.restosSearchedByRank[indexPath.row]
            }
            
            if let userLoc = self.currentUserLocation {
                cell.setRestaurant(some: resto, origin: userLoc)
            }
        }
        
        return _cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.currentSearchMode {
        case .Radius:
            return self.restosSearchedByRadius.count
        case .Ranked:
            return self.restosSearchedByRank.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.currentSearchMode {
        case .Radius:
            self.currentSelectedRestaurant = self.restosSearchedByRadius[indexPath.row]
        case .Ranked:
            self.currentSelectedRestaurant = self.restosSearchedByRank[indexPath.row]
        }
        
        if let _ = self.currentSelectedRestaurant {
            self.performSegue(withIdentifier: "showDetail", sender: self)
        }
    }
}

