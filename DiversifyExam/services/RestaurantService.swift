//
//  RestaurantService.swift
//  DiversifyExam
//
//  Created by Joseph Daryl Locsin on 08/11/2017.
//  Copyright © 2017 Joseph Daryl Locsin. All rights reserved.
//

import Foundation
import MapKit


class RestaurantService {
    
    private let BASE_URL = "https://maps.googleapis.com/maps/api/place"
    private let API_KEY = "AIzaSyAdJPUBI-5fuZkNGI_3FiwsENs-ucfz00I"
    
    enum SearchMode {
        case Radius(some: CLLocationDistance)
        case Ranked
    }
    
    enum RestaurantServiceError {
        case InvalidParameters
        case InvalidJSONReturn
        case MissingImageResponse
        case MissingDataResponse
        case NoResultsFound
        case None
    }
    
    func fetchThumbnail(restaurant: Restaurant, completion: @escaping ((UIImage, RestaurantServiceError)->())) {
        self.fetchImage(restaurant: restaurant, size: CGSize(width: 100, height: 100), completion: completion)
    }
    
    func fetchImage(restaurant: Restaurant, size: CGSize, completion: @escaping ((UIImage, RestaurantServiceError)->())) {
        
        let noImgAvail: UIImage! = UIImage(named: "no_img_avail")
        
        guard let ref = restaurant.photoReference else {
            completion(noImgAvail, .InvalidParameters)
            return
        }
        
        let urlString = "\(BASE_URL)/photo?photoreference=\(ref)&sensor=false&maxheight=\(Int(size.height))&maxwidth=\(Int(size.width))&key=\(API_KEY)"
        
        guard let url = URL(string: urlString) else {
            completion(noImgAvail, .InvalidParameters)
            return
        }
        
        print("[REQUEST (1)] url:\(urlString)")
        
        URLSession.shared.dataTask(with: url) { (someData, someResponse, someError) in
            if let _data = someData {
                if let image = UIImage(data: _data) {
                    completion(image, .None)
                } else {
                    completion(noImgAvail, .MissingImageResponse)
                }
            } else {
                completion(noImgAvail, .MissingDataResponse)
            }
        }.resume()
    }
    
    func fetchNearbyRestaurants(from: CLLocationCoordinate2D, searchString: String?, mode: SearchMode, completion: @escaping (([Restaurant]?, RestaurantServiceError)->())) {
        
        var urlString = "\(BASE_URL)/nearbysearch/json?location=\(from.latitude),\(from.longitude)&type=restaurant&key=\(self.API_KEY)"
        
        switch mode {
        case .Radius(let some):
            urlString = urlString.appending("&radius=\(some)")
            
        case .Ranked:
            urlString = urlString.appending("&rankby=distance")
        }
        
        if let _searchString = searchString, _searchString.characters.count > 0 {
            urlString = urlString.appending("&keyword=\(_searchString)")
        }
        
        guard let url = URL(string: urlString) else {
            completion(nil, .InvalidParameters)
            return
        }
        
        print("[REQUEST (2)] url:\(urlString)")
        
        URLSession.shared.dataTask(with: url) { (someData, someResponse, someError) in
            if let _data = someData {
                do {
                    let json = try JSONSerialization.jsonObject(with: _data, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if let thisDict = json as? [String:AnyObject] {
                        
                        print("[RESPONSE] forUrl:\(url.absoluteString) responseData:\(thisDict)")
                        
                        if let placesDict = thisDict["results"] as? [[String:AnyObject]] {
                            
                            var restos = [Restaurant]()
                            
                            for placeDict in placesDict {
                                if let name = placeDict["name"] as? String, let address = placeDict["vicinity"] as? String {
                                    if let geomDict = placeDict["geometry"] as? [String:AnyObject] {
                                        if let locDict = geomDict["location"] as? [String:AnyObject] {
                                            if let lat = locDict["lat"] as? Double, let lon = locDict["lng"] as? Double {
                                                
                                                let newResto = Restaurant(name: name, address: address, coordinates: CLLocationCoordinate2D(latitude: lat, longitude: lon))
                                                
                                                if let photoDicts = placeDict["photos"] as? [[String:AnyObject]] {
                                                    if let first = photoDicts.first {
                                                        if let ref = first["photo_reference"] as? String {
                                                            newResto.photoReference = ref
                                                        }
                                                    }
                                                }
                                                
                                                restos.append(newResto)
                                            }
                                        }
                                    }
                                }
                            }
                            
                            if restos.count > 0 {
                                completion(restos, .None)
                            } else {
                                completion(nil, .NoResultsFound)
                            }
                        } else {
                            completion(nil, .MissingDataResponse)
                        }
                    } else {
                        completion(nil, .MissingDataResponse)
                    }
                } catch  {
                    completion(nil, .InvalidJSONReturn)
                }
            } else {
                completion(nil, .MissingDataResponse)
            }
        }.resume()
    }
}
