//
//  RestaurantCell.swift
//  DiversifyExam
//
//  Created by Joseph Daryl Locsin on 08/11/2017.
//  Copyright © 2017 Joseph Daryl Locsin. All rights reserved.
//

import Foundation
import MapKit


class RestaurantCell: UITableViewCell {
    
    @IBOutlet private weak var viewThumbnail: UIImageView!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDescription: UILabel!
    @IBOutlet private weak var labelDistance: UILabel!
    
    func setRestaurant(some: Restaurant, origin: CLLocation) {
        
        self.viewThumbnail.image = UIImage(named: "no_img_avail") // default
        RestaurantService().fetchThumbnail(restaurant: some) { (someImage, someError) in
            DispatchQueue.main.async {
                self.viewThumbnail.image = someImage
            }
        }
        
        self.labelTitle.text = some.name
        self.labelDescription.text = some.address
        self.labelDistance.text = some.getMyPrettyDistance(withRespectTo: origin.coordinate)
    }
    
    static func cellHeight() -> CGFloat {
        return 44
    }
    
    static func cellIdentifier() -> String {
        return "RestaurantCell"
    }
}
