//
//  UIViewController+Extensions.swift
//  DiversifyExam
//
//  Created by Joseph Daryl Locsin on 08/11/2017.
//  Copyright © 2017 Joseph Daryl Locsin. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    func presentAlertMessage(someMessage: String, someTitle: String? = nil) {
        let vc = UIAlertController(title: someTitle, message: someMessage, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(vc, animated: true, completion: nil)
    }
}
