//
//  Restaurant.swift
//  DiversifyExam
//
//  Created by Joseph Daryl Locsin on 08/11/2017.
//  Copyright © 2017 Joseph Daryl Locsin. All rights reserved.
//

import Foundation
import MapKit


class Restaurant {
    
    typealias PhotoReference = String // string id for a photo used by Place Photo API
    
    var name: String
    var address: String
    var coordinates: CLLocationCoordinate2D
    var photoReference: PhotoReference?
    
    var location: CLLocation {
        return CLLocation(latitude: self.coordinates.latitude, longitude: self.coordinates.longitude)
    }
    
    init(name: String, address: String, coordinates: CLLocationCoordinate2D) {
        self.name = name
        self.address = address
        self.coordinates = coordinates
    }
}

extension Restaurant {
    
    func getMyDistance(withRespectTo some: CLLocationCoordinate2D) -> CLLocationDistance {
        let someLoc = CLLocation(latitude: some.latitude, longitude: some.longitude)
        let diffInMeters = self.location.distance(from: someLoc)
        return diffInMeters
    }
    
    func getMyPrettyDistance(withRespectTo some: CLLocationCoordinate2D) -> String {
        let diffInMeters = self.getMyDistance(withRespectTo: some)
        
        let nf = NumberFormatter()
        nf.maximumFractionDigits = 2
        nf.roundingMode = .ceiling // so user will expect more for less
        
        let diff = NSNumber(value: diffInMeters/1000)
        return "\(nf.string(from: diff)!) km"
    }
}
