//
//  DetailVC.swift
//  DiversifyExam
//
//  Created by Joseph Daryl Locsin on 08/11/2017.
//  Copyright © 2017 Joseph Daryl Locsin. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class DetailVC: UIViewController {
    
    var userLocation: CLLocation?
    var restaurant: Restaurant!
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var labelTitle: UILabel!
    @IBOutlet private weak var labelDistance: UILabel!
    @IBOutlet private weak var labelDescription: UILabel!
    @IBOutlet private weak var mapView: MKMapView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let region = MKCoordinateRegionMakeWithDistance(self.restaurant.coordinates, 1000, 1000)
        self.mapView.setRegion(region, animated: true)
    }
    
    private func configureUI() {
        RestaurantService().fetchImage(restaurant: self.restaurant, size: self.imageView.bounds.size) { (someImage, someError) in
            DispatchQueue.main.async {
                self.imageView.image = someImage
            }
        }
        
        self.labelTitle.text = self.restaurant.name
        self.labelDescription.text = self.restaurant.address
        
        if let _userLoc = self.userLocation {
            self.labelDistance.text = self.restaurant.getMyPrettyDistance(withRespectTo: _userLoc.coordinate)
        } else {
            self.labelDistance.text = nil
        }
        
        self.mapView.delegate = self
        let annot = RestoAnnotation(title: self.restaurant.name, subtitle: self.restaurant.address, coordinate: self.restaurant.coordinates)
        self.mapView.addAnnotation(annot)
    }
}

extension DetailVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
    }
}
